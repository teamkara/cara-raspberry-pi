import logging
import logging.handlers
from logging.handlers import RotatingFileHandler

formatter = logging.Formatter(
  '%(asctime)s [%(levelname)-5.5s]: %(message)s - %(filename)s:%(lineno)d on %(threadName)s',
  datefmt='%d-%m %H:%M')

# set up logging to console
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)

# set up logging to file
fileHandler = RotatingFileHandler('log.log', maxBytes=10 * 1000 * 1000,
                                  encoding='utf8', backupCount=5)
fileHandler.setFormatter(formatter)
fileHandler.setLevel(logging.DEBUG)

logging.root.setLevel(logging.DEBUG)
logging.root.addHandler(console)
logging.root.addHandler(fileHandler)