'''
Uses the TextToSpeechV1 class from Watson SDK
https://console.bluemix.net/docs/services/text-to-speech/index.html#about
'''
import logging
import subprocess
from os.path import join, dirname, abspath
from threading import Thread
from fire import Fire
from watson_developer_cloud import TextToSpeechV1
from pixels import lights
from detect_input_output import Speaker
from Queue import Queue

logger = logging.getLogger(__file__)

semaphore = Queue()

# Instantiate the conversation class
text_to_speech = TextToSpeechV1(
    username='8e16b598-7aaf-4ac0-8033-95efa940aa62',
    password='ZuxQVqwLcPBA'
)


def speak(message):
    '''
    Run the function with the given message.
    For example: 'hello'
    '''
    thread = Thread(target=run, args=(message,))
    thread.start()


def run(message):
    if message == '': return
    logger.info('Speaking: %s', message)
    # Set the global flag
    semaphore.put_nowait(True)
    # Get the directory of this file
    current_dir = dirname(abspath(__file__))
    # Create a sound file to hold the speech in the given folder
    output_file = join(current_dir, '../resources/speech.wav')
    # Open the file in mode write binary
    with open(output_file, 'wb') as audio_file:
        # Ask Watson to speak the message in Kate's voice
        output = text_to_speech.synthesize(
          message,
          accept='audio/wav',
          voice='en-GB_KateVoice'
          )
        # Write the speech to the file
        audio_file.write(output)
        cmd = 'exec aplay -D {} {}'.format(Speaker, output_file)
        # Playback the speech by calling `aplay`
        # subprocess.call('exec aplay -D hw:1,0 {}'.format(output_file), shell=True)
        lights.speak()
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        # Clear the global flag
        lights.off()
        semaphore.get_nowait()
        logger.debug('Finished speaking')


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python text_to_speech.py "Hello, I'm Evelina"
    Calls the `speak` function with the given message.
    '''
    Fire(speak)
