'''
Uses the ConversationV1 class from Watson SDK.
https://console.bluemix.net/docs/services/conversation/index.html#about
'''
import logging
import json
from threading import Thread
from fire import Fire
from watson_developer_cloud import ConversationV1

logger = logging.getLogger(__file__)

# Instantiate the conversation class
conversation = ConversationV1(
    username='0db3cffb-b2d0-4ea9-aa76-f483775d33d6',
    password='30wEmEDvdc3D',
    version='2017-04-21'
)

# The conversation workspace identifier
workspace_id = '49d7e8c0-2cf3-4c21-beb4-10204a4bf8fd'

# Remember the context of the conversation
conversation_context = {
    workspace_id: None
}


def send_message(message):
    '''
    Run the method with the given message.
    For example: 'hello'
    '''
    logger.debug('Sending conversation message %s', message)
    if message == '':
        response = conversation.message(workspace_id=workspace_id)
    else:
        response = conversation.message(
            workspace_id=workspace_id,
            input={
            'text': message
            },
            context=conversation_context[workspace_id]
        )
    # Update the conversation context after a response
    conversation_context[workspace_id] = response['context']
    # Extract the text response
    response_text = response['output']['text']
    if len(response_text) > 0:
        # Print the response in a readable manner
        logger.debug(json.dumps(response_text, indent=2))
        return response_text[0]
    else:
        return ''


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python conversation.py 'Who are you'
    Calls the `send_message` function with the given message.
    '''
    Fire(send_message)
