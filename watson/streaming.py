'''
Speech to text transcription in real-time using IBM Watson.
https://github.com/Nagasaki45/watson-streaming/blob/master/watson_streaming.py
'''
import logging
import json
import ssl
import requests
import sounddevice
import signal
import websocket
from websocket import WebSocketConnectionClosedException
from threading import Thread
from os.path import join, dirname
from fire import Fire
from detect_input_output import Mic
from text_to_speech import semaphore

logger = logging.getLogger(__file__)

# Disable logging from the request library
logging.getLogger('requests.packages.urllib3.connectionpool').setLevel(logging.ERROR)

AUTH_API = 'https://stream.watsonplatform.net/authorization/api/'
STT_API = 'https://stream.watsonplatform.net/speech-to-text/api/'
WS_URL = 'wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize'

AUDIO_OPTS = {
    'dtype': 'int16',
    'samplerate': 44100,
    'channels': 1
}

BUFFER_SIZE = 2048

SETTINGS = {
    'inactivity_timeout': -1,  # Don't kill me after 30 seconds
    'interim_results': True,
}

def audio_gen():
    """
    Generate audio chunks.
    """
    AUDIO_OPTS['device'] = Mic
    with sounddevice.RawInputStream(**AUDIO_OPTS) as stream:
        while True:
            if semaphore.empty():
                chunk, _ = stream.read(BUFFER_SIZE)
                yield chunk[:]  # To get the bytes out of the CFFI buffer


def send_audio(ws):
    """
    Get chunks of audio and send them to the websocket.
    """
    for chunk in audio_gen():
        ws.send(chunk, websocket.ABNF.OPCODE_BINARY)


def on_error(ws, error):
    logger.error('WebSockets error %s', error)


def on_close(ws):
    logger.warning('WebSockets connection closed')


def start_communicate(ws, settings):
    """
    Send the initial control message and start sending audio chunks.
    """
    logger.info('WebSockets connection opened')

    settings.update({
        'action': 'start',
        'content-type': 'audio/l16;rate={samplerate}'.format(**AUDIO_OPTS),
    })

    # Send the initial control message which sets expectations for the
    # binary stream that follows:
    ws.send(json.dumps(settings).encode('utf8'))
    # Spin off a dedicated thread where we are going to read and
    # stream out audio.
    try:
        t = Thread(target=send_audio, args=(ws, ))
        t.daemon = True  # Not passed to the constructor to support python 2
        t.start()
    except (KeyboardInterrupt, SystemExit, WebSocketConnectionClosedException):
        logger.debug('Received keyboard interrupt, stopping streaming.')
        ws.stop()

def parse_credentials(credentials_file):
    file_path = join(dirname(__file__), credentials_file)
    with open(file_path) as f:
        return json.load(f)['speech_to_text'][0]['credentials']


def obtain_token(credentials):
    params = {
        'url': STT_API,
    }
    auth = (credentials['username'], credentials['password'])
    url = AUTH_API + '/v1/token'
    response = requests.get(url, params=params, auth=auth)
    return response.text


def transcribe(callback, settings):
    """
    Main API for Watson STT transcription.

    callback:         function to call when Watson sends us messages.
    settings:         dictionary of input and output features.
    credentials_file: path to 'credentials.json'.
    """
    credentials_file = 'credentials.json'
    credentials = parse_credentials(credentials_file)
    token = obtain_token(credentials)

    def callback_wrapper(ws, msg):
        data = json.loads(msg)
        return callback(data)

    ws = websocket.WebSocketApp(
        WS_URL,
        header={'X-Watson-Authorization-Token': token},
        on_open=lambda ws: start_communicate(ws, settings),
        on_message=callback_wrapper,
        on_error=on_error,
        on_close=on_close,
    )

    ws.run_forever(sslopt={'cert_reqs': ssl.CERT_NONE})


def default_callback(data):
    """
    Nicely print received transcriptions.
    """
    if 'results' in data:
        transcript = data['results'][0]['alternatives'][0]['transcript']
        logger.debug(transcript)


def transcribe_test():
    transcribe(default_callback, SETTINGS)        


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python streaming.py transcribe_test
    Calls the `stream` function before you start speaking into the mic.
    '''
    Fire({
        'transcribe': transcribe,
        'transcribe_test': transcribe_test
    })
