'''
Uses the SpeechToTextV1 class from Watson SDK.
https://console.bluemix.net/docs/services/speech-to-text/index.html#about
'''
import logging
import json
from threading import Thread
from fire import Fire
from watson_developer_cloud import SpeechToTextV1

logger = logging.getLogger(__file__)

# Instantiate the conversation class
speech_to_text = SpeechToTextV1(
    username='06bc9ad8-26b5-4179-b527-75b5b701bea7',
    password='cqRdBeWI10ZB'
)


def transcribe_file(speech_file):
    '''
    Run the function with the given audio file.
    For example: ../resources/speech_input.wav
    '''
    thread = Thread(target=run, args=(speech_file,))
    thread.start()


def run(speech_file):
    logger.debug('Transcribing speech in %s', speech_file)
    # Open the file in mode write binary
    with open(speech_file, 'rb') as audio_file:
        # Ask Watson to transcribe the speech into text
        transcript = speech_to_text.recognize(
          audio_file,
          content_type='audio/wav'
        )
        # Print the result in a readable format
        logger.debug(json.dumps(transcript, indent=2))
        return transcript


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python speech_to_text.py ../resources/speech.wav
    Calls the `transcribe_file` function with the given message.
    '''
    Fire(transcribe_file)
