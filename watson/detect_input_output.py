import logging
import json
import sys
import pyaudio
from pyaudio import PyAudio
from fire import Fire

logger = logging.getLogger(__file__)
p = PyAudio()

def is_microphone(device):
    return device.get('maxInputChannels') > 0 and 'seeed-4mic-voicecard' in device.get('name')

def is_speaker(device):
    name = device.get('name')
    return device.get('maxOutputChannels') > 0 and ('ALSA' in  name and 'HDMI' not in name)
                 
def hw(device):
    n = max(0, device.get('index') - 1)
    return 'hw:{},{}'.format(n, device.get('hostApi'))

def detect_mic():
    for h in range(0, p.get_host_api_count()):
        print(h)
        hostinfo = p.get_host_api_info_by_index(h)
        numdevices = hostinfo.get('deviceCount')
        for i in range(0, numdevices):
            device = p.get_device_info_by_host_api_device_index(h, i)
            info = json.dumps(device, indent=2)
            if is_microphone(device):
                logger.debug('Mic: %s - %s\n', hw(device), info)
                return hw(device)

    logger.error('Microphone not detected')
    sys.exit(1)

def detect_speaker():
    for h in range(0, p.get_host_api_count()):
        hostinfo = p.get_host_api_info_by_index(h)
        numdevices = hostinfo.get('deviceCount')
        for i in range(0, numdevices):
            device = p.get_device_info_by_host_api_device_index(h, i)
            info = json.dumps(device, indent=2)
            if is_speaker(device):
                logger.debug('Speaker: %s - %s\n', hw(device), info)
                return hw(device)

    logger.error('Speaker not detected')
    sys.exit(1)

Mic = detect_mic()
Speaker = detect_speaker()

if __name__ == '__main__':
    Fire({
      'detect_mic': detect_mic,
      'detect_speaker': detect_speaker
    })