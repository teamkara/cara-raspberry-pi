*Note: This guide is WIP (work in progress)*

# Hello, I'm Cara! 

_My name means _'friend'_ in an [old Irish dialect](https://en.wiktionary.org/wiki/cara#Irish) and I'm here to help you manage your mental wellbeing. Not to worry, I know what I'm doing. It's called [Cognitive Behavior Therapy.](https://www.nhs.uk/conditions/cognitive-behavioural-therapy-cbt/)_

---

> "Mental care is among the medical disciplines that are most actively embracing artificial intelligence solutions. Chatbots are said to be the state-of-the-art work tool dedicated to our emotional wellness"

---

## Resources

+ [Conversational UI Principles — Complete Process of Designing a Website Chatbot](https://medium.com/swlh/conversational-ui-principles-complete-process-of-designing-a-website-chatbot-d0c2a5fee376)
+ [Making Chatbots Talk — Writing Conversational UI Scripts Step by Step](https://uxdesign.cc/making-chatbots-talk-writing-conversational-ui-scripts-step-by-step-62622abfb5cf)
+ [MENTAL HEALTH CHATBOTS - THE FUTURE OF THERAPY?](http://www.himssinsights.eu/mental-health-chatbots-future-therapy)


# The Tech Bits

## General Rasbperry Pi setup

You can connect to a Raspberry Pi with a keyboard, a mouse and a monitor. For conveniece, you can connect through your existing computer using
a *remote desktop* software like [RealVNC Viewer](https://www.realvnc.com/en/connect/download/viewer/).
The advantages are obvious. For example, you can copy-paste text or share files directly between your host computer and the Pi.
The disadvantage is that it can be slower than a direct connection. Sometimes you may need to take extra steps to ensure the [correct resolution](https://support.realvnc.com/knowledgebase/article/View/523/2/troubleshooting-vnc-server-on-the-raspberry-pi) in the Viewer.

Another form of remote connection is through the [ssh protocol](https://www.ssh.com/ssh/protocol/). This is done through the terminal, so no graphical interface. Good for advanced users because of the speed. For example, if both the Pi
and your computer are connected to the same wifi network, you can use the command `ssh pi@ip` to connect, where `ip` is the IP of your Raspberry.

How I 'hacked' the Pi to connect to the `eduroam` wifi (it's disabled in the graphical interface): 

- `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`
- add the following and save (`Ctrl+O, Ctrl+X`)
```sh
network={
       ssid="eduroam"
       key_mgmt=WPA-EAP
       identity="uclname@ucl.ac.uk"
       password="yourpassword"
}
```

**Initial Setup**

1. Make sure Raspbian OS is installed and up to date with `sudo rpi-update`
2. Enable VNC and SSH in Raspberry Pi Preferences
3. Install [RealVNC Viewer](https://www.realvnc.com/en/connect/download/viewer/) on your computer and create an account.
4. Login with the same account on the Raspberry side in the VNC server.
5. On the Pi, set the [correct resolution](https://support.realvnc.com/knowledgebase/article/View/523/2/troubleshooting-vnc-server-on-the-raspberry-pi) in the Viewer.

**Required Hardware**

1. [Respeaker 4-Mic Array](https://shop.pimoroni.com/products/respeaker-4-mic-array-for-raspberry-pi): a microphone for voice applications plus a bunch of controllable leds, all in one - **£22.50**
2. Setup the drivers for Respeaker following the instructions [here](https://github.com/respeaker/seeed-voicecard).
3. [Switchable USB hub](https://www.yepkit.com/product/300115/YKUSHXS) to control the usb aromatherapy stick by turning on/off the USB port - **€24.99**
4. [Capacitive touch board](https://learn.adafruit.com/mpr121-capacitive-touch-sensor-on-raspberry-pi-and-beaglebone-black/overview) for making music by attaching wires to conductive stuff like aluminium - **£15.50**
5. [Extension HAT](https://www.modmypi.com/raspberry-pi/iousbanalogue-expansion-1028/gpio-expansion-boards-1029/52pi-triple-gpio-multiplexing-expansion-board) to connect all this hardware on a single Pi - **£16.00**
6. [Cheap speaker](https://www.amazon.co.uk/gp/product/B002CS2T4I/ref=oh_aui_detailpage_o06_s00) - **£5.95**

Grand Total Hardware (including wires): **£85.00**


**Sharing the source code**

To share the source code and access to several services like the IBM Watson console, I created a Gmail account `carathechatbot@gmail.com` and shared it the team.

The source code resides in `Bitbucket`, a version control repository from Atlassian, very similar to GitHub. Unlike GitHub, it offers **private** repositories _for free_.

> It is extremely useful to familiarize yourself with [Git](https://juristr.com/blog/2013/04/git-explained/). Please read about it ahead of our meeting :)

Terminology to know:

- **pull** from repository means to get the freshest copy of the code that has been shared
- **push** to the repository means to share your code with the rest
- **checkout** or **clone** a repository means to download a version of the code on your computer to work with it, before you _push_ it for everyone to see :)

**Steps to run the project on the Pi**

1. Make sure Python 3 is installed and default environment. Check [this tutorial](https://stackoverflow.com/a/18425592/312778).
2. Install `watson-developer-cloud` Python library by running `pip install --upgrade watson-developer-cloud` on your laptop and Raspberry Pi
3. Look at the [code examples](https://github.com/watson-developer-cloud/python-sdk/tree/master/examples)
4. Create [ssh keys](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) and set the key in Bitbucket account.
5. Clone the repo: `git clone git@bitbucket.org:carathechatbot/cara-raspberry-pi.git`
6. Highly recommended: install [Visual Studio Code](https://code.headmelted.com/) on Raspberry Pi 
7. Install the sound drivers and other libraries needed like `sudo apt-get install libasound2-dev`; there are so many now that the only way to remember is to check the error because it tells you what you need to install; also spend a few hours checking online;
8. Run `python bot.py`
9. Trying the first prototype :)

![First Prototype](http://imageshack.com/a/img924/6740/pDG9x8.gif "Good enough for me")


> Remember: [StackOverflow](https://stackoverflow.com/) is the new Google for programmers.

Bonus for me: while I have been coding for 13 years now in different languages, this is my second time coding in Python for more than just a script or two. I am familiarizing myself with Python 3. To get to an advanced level in any language, I need to understand "the best practices" of that ecosystem, for example, [this resource](http://docs.python-guide.org/en/latest/writing/structure/) is great.


## IBM Watson Setup

Our [IBM Dashboard](https://console.bluemix.net/dashboard/apps/)

**Watson Conversation Explained**

### Intents

> An intent is a group of examples of things that a user might say to communicate a specific goal or idea. 

To identify intents, start with something that a user might want and then list the ways that the user might describe it. For each intent, think of the various ways that a user might express his or her desire—those are the examples. Examples can be developed by using a crowdsourcing approach.

For example, in a discussion with the support team, you might gather this set of standard questions that support received from users:

+ What is the status of the business application? I could not access it.
+ How to get access to a business application?
+ How to reset my password for a specific application?
+ When to renew my workstation?
+ How to bring my own device and connect it to enterprise network?

Each of those questions is documented as a frequently asked question in the support team's document repository. Some solutions persist in a relational database in the form of application > problem > solution.

Based on the questions, you can extract these intents:

+ Access to a business applications like expense report, AbC.
+ Reset password
+ Access to supplier on boarding business process
+ Bring your own device


### Entities

> An entity is a portion of the user's input that you can use to provide a different response to a particular intent.

Adding values and synonyms to entities helps your chatbot learn important details that your users might mention.

Each entity definition includes a set of specific entity values that can be used to trigger different responses. Each value can have multiple synonyms that define different ways that the same value can be specified in user input.

### Dialog

> After you specify your intents and entities, you can construct the dialog flow. A dialog is made up of nodes that define steps in the conversation.

### Difference between Conversation and Natural Language Understanding (NLU)

The Conversation service is separate from NLU. Conversation is about building a chatbot on your own domain. The intents/entities are only what you train it on, and the dialog is a feature only available in conversation, not NLU.

NLU is a pretrained service that returns various information back about text, but _does not do anything with a response_, and will give you back what it has been pretrained on. Out of the box, you can't change this. You can use a product like [Watson Knowledge Studio](https://www.ibm.com/watson/services/knowledge-studio/) to train a custom annotator, but NLU itself knows what it knows and thats it.

There is no need to combine these, but it is possible. Depending what problem youre trying to solve will help guide you in which you want to use. If you want to understand data about unstructured text, with no real training time required, NLU is right for you. _If you want to develop a chatbot to help your users with some problem, Conversation is right for you._

If you want to build a chatbot about generic things, or if you require things like people's name, extracting locations around the world, etc, and respond accordingly, you could use NLU to extract the metadata, and then pass that to Conversation and in conjunction with your custom intents/entities/dialog have a more powerful conversation.


### Documentation
1. Watson [Node SDK](https://github.com/watson-developer-cloud/node-sdk) 
2. TTS (Text-To-Speech) [API](https://www.ibm.com/watson/developercloud/text-to-speech/api/v1/)
3. TTS [documentation](https://console.bluemix.net/docs/services/text-to-speech/getting-started.html#gettingStarted)
4. How to create a [conversation](https://www.ibm.com/cloud/garage/tutorials/watson_conversation_support)
5. Watson [NLU + Conversation](https://developer.ibm.com/dwblog/2017/chatbot-watson-conversation-natural-language-understanding-nlu/)
6. Comprehensive [comparison of chatbots](https://chatbotsjournal.com/25-chatbot-platforms-a-comparative-table-aeefc932eaff) 