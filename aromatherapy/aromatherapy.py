import subprocess
import logging
from fire import Fire
from threading import Thread

# Instantiate a logger for this file
logger = logging.getLogger(__file__)

# The hardware command for the switchable USB board
CMD = 'sudo ykushcmd ykushxs'

# The available options for the command above and their descriptions
OPTIONS = {
    'list': ['-l', 'Listing connected hardware'],
    'status': ['-g', 'Check hardware status'],
    'on': ['-u', 'Turning on'],
    'off': ['-d', 'Turning off']
}


def switch(option_name):
    '''
    Get the corresponding option for the given option name.
    E.g.: 'list' corresponds to option -l
    Execute the command with the option and log the success or error status.
    '''
    option = OPTIONS[option_name][0]  # Get the option for the give name
    logger.debug(OPTIONS[option_name][1])  # Print the option description
    try:
        subprocess.call('exec {} {}'.format(CMD, option), shell=True)
        logger.debug('Success!')
    except Exception as e:
        logger.error('Exception: %s', e)


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python aromatherapy.py switch on
    Calls the `switch` function with the given option
    '''
    Fire(switch)
