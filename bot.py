import aromatherapy
import beetbox
import watson
import logging
import logger
import time
import signal
from fire import Fire
from pixels import lights
from threading import Thread


BOT_NAME = 'Katie'

AROMATHERAPY = {
    'on':  ['turn on the aromatherapy', 'turn it on'],
    'off': ['turn off the aromatherapy', 'turn it off']
}

MUSIC = {
    'piano':  ['play piano', 'play something nice', 'play some music'],
    'drums': ['play drums', 'make some music', 'make some noise'],
}

SPEECH_SETTINGS = {
    'inactivity_timeout': -1,  # Don't kill me after 30 seconds
    'interim_results': True,
}

logger = logging.getLogger(__file__)


def lights_listen():
    try:
        lights.listen()
        time.sleep(5)
        lights.off()
    except (KeyboardInterrupt, SystemExit):
        lights.off()


def transcript_callback(data):
    if 'state' in data:
        status = data['state']
        logger.info('Status is %s', status)

    if 'results' in data.keys():
        result = data['results'][0]
        if result['final']:
            transcript = result['alternatives'][0]['transcript']
            on_trascript(transcript)


def on_trascript(phrase):
    logger.info('%s heard: %s', BOT_NAME, phrase)
    process_command(phrase)

    # Listen for a while then turn the lighs off
    try:
        lt = Thread(target=lights_listen, args=())
        lt.start()
    except KeyboardInterrupt:
        pass

    response = watson.send_message(phrase)
    response = response.lower()
    process_command(response)
    watson.speak(response)


def is_command(cmds, response):
    return any(c in response for c in cmds)


def process_command(phrase):
    for key, value in AROMATHERAPY.items():
        if is_command(AROMATHERAPY[key], phrase):
            logger.debug('Turning %s aromatherapy', key)
            aromatherapy.switch(key)
    for key, value in MUSIC.items():
        if is_command(MUSIC[key], phrase):
            logger.debug('Music %s', key)
            beetbox.make_music(key)
    
    
def start():
    logger.info('%s is alive!', BOT_NAME)
    aromatherapy.switch('off')
    # send an empty message to start the conversation
    watson.speak(watson.send_message(''))
    time.sleep(2)
  
    while True:
        try:
            watson.transcribe(transcript_callback, SPEECH_SETTINGS)
            signal.pause()
        except (KeyboardInterrupt, SystemExit):
            logger.info('Bye!')
            watson.speak('Bye')
            break


if __name__ == '__main__':
    '''
    Available commands:
    python bot.py aromatherapy switch [on | off]
    python bot.py beetbox make_music [piano | drums]
    python bot.py watson send_message 'Who are you'
    python bot.py watson speak "Hello, I'm Sophie"
    python bot.py watson transcribe_file resources/speech.wav
    python bot.py watson transcribe_test
    '''
    Fire({
        'aromatherapy': aromatherapy,
        'beetbox': beetbox,
        'watson': watson,
        'start': start
    })
