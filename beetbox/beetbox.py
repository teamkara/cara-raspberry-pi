import subprocess
import sys
import os
import logging
import Adafruit_MPR121.MPR121 as MPR121
import time
from os.path import dirname, abspath, join
from threading import Thread
from fire import Fire

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/..")
from watson.detect_input_output import Speaker

# Disable logging from the I2C library
logging.getLogger('I2C').setLevel(logging.ERROR)


logger = logging.getLogger(__file__)

'''
The Raspberry Pi sometimes doesn't load all the hardware modules on start.
If there is an error running this program, try to force load it with:
`sudo modprobe i2c_bcm2708`
Check more about it here: https://goo.gl/wBaxqV
'''
os.system('sudo modprobe i2c_bcm2708')

# The available sounds by instrument
SOUNDS = {
    'drums': ['kick', 'snare', 'open', 'closed', 'clap', 'cymbal'],
    'piano': ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1']
}

# The template for a sound file
SOUND_FILE = 'sounds/{instrument}/{sound}.wav'  

# Create the touch sensor instance
touch = MPR121.MPR121()

current_dir = dirname(abspath(__file__))

if not touch.begin():
    '''
    Initialize communication with the touch board.
    If the board cannot start, terminate the program.
    '''
    logger.error('Error initializing MPR121. Check your wiring!')
    sys.exit(1)


def make_music(instrument):
    t = Thread(target=run, args=(instrument,))
    t.daemon = True  # Not passed to the constructor to support python 2
    t.start()


def run(instrument):
    # Print which instrument is playing
    logger.debug('Playing %s on %s', instrument, Speaker)
    # Get the last touched pin
    last_touched = touch.touched()
    # Run this until exiting the program by pressing `Ctrl-C`
    while True:
        try:
            # Get the currently touched pin
            current_touched = touch.touched()
            # Go through each pin
            for i in [3,4,5,6,7,8]:
                

                # Each pin is represented by a bit in the value:
                # 1 means touched, 0 means released
                pin_bit = 1 << i

                # If we touched the current pin and released the previous
                if current_touched & pin_bit and not last_touched & pin_bit:
                    # Get the corresponding sound for the pin, e.g. 'clap' or 'a1'
                    sound = SOUNDS[instrument][i % 6]
                    # Get the sound file corresponding to the sound
                    sound_file = join(current_dir, SOUND_FILE.format(instrument=instrument, 
                                                                    sound=sound))
                    # Print which pin and which sound is playing
                    logger.debug('Pin %d touched', i)
                    # Play the sound by executing a command called `aplay`
                    cmd = 'exec aplay -D {} {}'.format(Speaker, sound_file)
                    logger.debug('Executing %s', cmd)
                    subprocess.call(cmd, shell=True)

                # If we released the current pin
                if not current_touched & pin_bit and last_touched & pin_bit:
                    logger.debug('Pin %d released', i)
            # Update the previous pin before repeating again
            last_touched = current_touched
            time.sleep(0.1)
        
        except (KeyboardInterrupt, SystemExit):
            logger.debug('Received keyboard interrupt, stopping streaming.')
            break


if __name__ == '__main__':
    '''
    This is triggered when this file is run as a script, e.g.:
    python beetbox.py make_music piano
    Calls the `make_music` function with the given instrument.
    '''
    Fire(make_music)
